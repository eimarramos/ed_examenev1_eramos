import java.util.Scanner;

public class Examen1Evaluacion {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String linea, palabra;
		int numero, numerosTriangulo = 0;
		Double numeroDouble, total = 0.0;
		char[] caracteres;
		boolean esPositivo = true, salir = true;
		int veces = 0;

		while (salir) {
			
			System.out.println("\n====\nMENU\n====");
			System.out.println("1. Palabra invertida");
			System.out.println("2. Tri�ngulo n�meros");
			System.out.println("3. Mayor/Menor");
			System.out.println("4. Salir");
			System.out.print("Introduce una opcion: ");
			linea = scan.nextLine();
			numero = Integer.parseInt(linea);
			switch (numero) {
			case 1:
				System.out.print("Introduce una palabra: ");
				palabra = scan.nextLine();
				System.out.print("Tu palabra invertida es: ");
				System.out.print("");
				caracteres = palabra.toCharArray();
				for (int i = caracteres.length - 1; i >= 0; i--) {
					System.out.print(caracteres[i]);
				}
				break;
			case 2:
				System.out.print("Introduce el tama�o del triangulo: ");
				linea = scan.nextLine();
				numero = Integer.parseInt(linea);
				while (numero < 1) {
					System.out.print("\n Debes de introducir un numero positivo: ");
					linea = scan.nextLine();
					numero = Integer.parseInt(linea);
				}

				for (int i = 1; i < numero + 1; i++) {

					for (int j = numero + 1; j > i; j--) {
						numerosTriangulo += 1;
						System.out.print(numerosTriangulo + " ");
					}
					System.out.println("");
					numerosTriangulo = 0;
				}
				break;
			case 3:

				System.out.println("Introduce un n�mero (Negativo para salir)");
				linea = scan.nextLine();
				numeroDouble = Double.parseDouble(linea);
				if (numeroDouble < 1) {
					System.out.println("No has introducido ningun numero valido...");
					break;
				}
				while (esPositivo) {
					total += numeroDouble;
					veces += 1;
					System.out.println("Introduce un n�mero (Negativo para salir)");
					linea = scan.nextLine();
					numeroDouble = Double.parseDouble(linea);

					if (numeroDouble < 1) {
						esPositivo = false;
					}
				}
				System.out.println("La suma de los n�meros es : " + total);
				System.out.println("La media de los n�meros es :" + (total / veces));
				break;
			case 4:
				System.out.println("Saliendo...");
				salir = false;
				break;
			default:
				System.out.println("\n Opcion incorrecta");
				break;

			}
		}
	}

}
